Automatic cache tag invalidation for referenced nodes

ie. If you have a node that contains a node reference field... the referenced
node will have its cache invalidated if the node or nodes referencing
it are updated.

Instructions... Install the module and it does the rest.

Feel free to submit pull requests.

Thanks, Alloy Lab
